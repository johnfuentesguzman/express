const express = require('express');
const path = require('path');
const router = express.Router();

// using get,post, put, etc it means that for seeing the path content it must be 100% matched with the method and the path itself
router.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, '..', 'views', 'shop.html')); // path func and __dirname = resolving a absolute path (including Operative system  into the path)
})


module.exports = router;