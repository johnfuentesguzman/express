const express = require('express');
const path = require('path');

const router = express.Router();
const rootDirectory = require('../util/path');

router.get('/add-product', (req, res) => {
    console.log(path.dirname(__filename));
    // '../' = we are not 100% sure that i will work in all operative system, due to that i use '..'
    res.sendFile(path.join(path.dirname(__filename), '..', 'views', 'add-product.html')); // path func and __dirname = resolving a absolute path (including Operative system  into the path)
})

router.post('/product', (req, res) => {
    console.log(req.body)
    res.redirect('/');
});

module.exports = router;