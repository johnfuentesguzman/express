
const http = require('http');
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser'); // Allow to handle the incoming request body
const app = express();

const adminRoutes = require('./routes/admin');
const shopRoutes = require('./routes/shop');

app.use(bodyParser.urlencoded({extended: false})); // get the http request parameters in the URL
app.use(express.static(path.join(__dirname, 'public'))); // serving static file (no handle by express router), this case .css files

app.use('/admin', adminRoutes); // importing my custom routes file, and include a especific prefix path for all the routes into that file
app.use(shopRoutes); 

// app.use((req, res, next)=> { // request, response, and next: its a parameter function that allows the request to continue to next middleware behaviour
//     next();
// });

app.use('/', (req, res, next) => {
    // res.status(404).send('<h4>Page Not Found</h4>');
    res.status(404).sendFile(path.join(__dirname, 'views', '404.html'));
})
app.listen(3000)
